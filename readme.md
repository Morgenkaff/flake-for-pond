# A flake for Pond by [Alice](https://gitlab.com/alice-lefebvre):

Moved to: https://codeberg.org/morgenkaff/flake-for-pond

## - a soothing in-terminal idle screen

Url: https://gitlab.com/alice-lefebvre/pond

![Screenshot of pond in a terminal](https://gitlab.com/alice-lefebvre/pond/-/raw/main/images/pond.png)

A software that simulates a little pond, complete with flowering lilypads and adorable little frogs jumping around.

## How to use:

Add this lines to the inputs of your flake:

```
    pond.url = "gitlab:Morgenkaff/flake-for-pond";
```

Add `pond` to `outputs` in the flake, to `specialArgs` or `home-manager.extraSpecialArgs` and input in your configuration or home-manager.

Lastly `pond` to the `environment.systemPackage` or `home.packages`, depending on your setup.
Example:

```
home.packages = with pkgs; [

    pond

    dig
    bottom
    ncdu
    lshw
    wget
    tcpdump
    traceroute
    neofetch
    micro
    imagemagick

    # USB stick recovery
    foremost # https://foremost.sourceforge.net/

    # Development (general dev tools used on whole system)
    hugo

];
```
