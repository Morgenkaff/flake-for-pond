{
  description = "A flake for Pond";
  
  outputs = { self, nixpkgs }:
    let

      # List of supported systems:
      supportedSystems = [
        "x86_64-linux"
        "aarch64-linux"
        "x86_64-darwin"
        "aarch64-darwin"
      ];
      # Helper function to generate an attrset '{ x86_64-linux = f "x86_64-linux"; ... }'.
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      # Nixpkgs instantiated for supported system types.
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });
      pkgs = nixpkgs;

    in
    {
      # Provide some binary packages for selected system types.
      packages = forAllSystems (system:
        let 
          pkgs = nixpkgsFor.${system};
        in
        {
          # Declare a default package (there's only one package in this flake..)
          default = self.packages.${system}.pond;

          pond = with pkgs; pkgs.stdenv.mkDerivation rec {
            name = "pond";
            pname = "pond";
            src = fetchFromGitLab {
              owner = "alice-lefebvre";
              repo = "pond";
              rev = "1b74089f0d44f13efe8f695849d7cb8c7c6643de";
              hash = "sha256-xG2dQ0hzQMNGV2NreLzXQWeDE5QJc0j6A5JBXmSMavk=";
            };
            nativeBuildInputs = [
              ncurses
              gcc
            ];
            patchPhase = ''
              sed -i 's/lcurses/lncurses/g' Makefile
              sed -i 's/o bin\/pond/o pond/g' Makefile

              sed -i 's/install: bin\/pond/install : pond/g' Makefile
              sed -i '/rm -f \/usr\/local\/games\/pond/d' Makefile
              sed -i '/cp bin/c\	cp pond bin/pond' Makefile


              sed -i '/uninstall/,$d' Makefile
              '';
            buildPhase = ''
              make
            '';
            installPhase = ''
              mkdir -p $out/bin
              mv pond $out/bin/
            '';
        };
          

        }
      );

      # At last declaring the overlay used in the recieving/consuming flakes
      overlays.default = final: prev: {
        pond = self.packages.${prev.system}.pond;
      };

    };
}
